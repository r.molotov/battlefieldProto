﻿using UnityEngine;
using System.Collections;

public class CarLights_Matveev : MonoBehaviour {

    public Renderer[] brakeLights;
    public Renderer[] headLights;
    public Renderer[] turnLights;
	public Material brakeLightON;
	public Material brakeLightOFF;
	public Material TurnLightON;
	public Material TurnLightOFF;
	public Material headLightsON;
	public Material headLightsOFF;
	public Light SpolightLEFT;
	public Light SpolightRIGHT;
    private bool rightSignalON = false;
    private bool leftSignalON = false;
    private bool Fari = false;
	void Start() {
		
	
	}
	
	void Update() {

		if (Input.GetKey(KeyCode.S) || Input.GetKey("space"))
		{
				brakeLights[0].material = brakeLightON;
                brakeLights[1].material = brakeLightON;
		}
		else {
                brakeLights[0].material = brakeLightOFF;
                brakeLights[1].material = brakeLightOFF;
		}
	
		if (Input.GetKey(KeyCode.D))
		{
            turnLights[0].material = TurnLightON;
            turnLights[1].material = TurnLightON;
            rightSignalON = true;
        }
		else {
            turnLights[0].material = TurnLightOFF;
            turnLights[1].material = TurnLightOFF;
            rightSignalON = false;
        }
		
		
		if(Input.GetKey(KeyCode.A))
		{
            turnLights[2].material = TurnLightON;
            turnLights[3].material = TurnLightON;
            leftSignalON = true;

        }
		else {
            turnLights[2].material = TurnLightOFF;
            turnLights[3].material = TurnLightOFF;
            leftSignalON = false;
        }

        if (TurnLightON)
        {
            float floor = 0f;
            float ceiling = 1f;
            float emission = floor + Mathf.PingPong(Time.time * 2f, ceiling - floor);
            if (rightSignalON == true)
            {
                turnLights[0].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
                turnLights[1].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
            }
            else if (leftSignalON == true)
            {
                turnLights[2].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
                turnLights[3].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
            }
        }

        if (Input.GetKeyDown("1") && !Fari)
		{
			headLights[0].material = headLightsON;
			headLights[1].material = headLightsON;
			SpolightLEFT.intensity = 5f;
			SpolightRIGHT.intensity = 5f;
		}
		
		if(Input.GetKeyDown("1") && Fari){
			headLights[0].material = headLightsOFF;
			headLights[1].material = headLightsOFF;
			SpolightLEFT.intensity = 0f;
			SpolightRIGHT.intensity = 0f;
		}
        if (Input.GetKeyDown("1"))
        {
            Fari = !Fari;
        }
    }
}
