﻿using UnityEngine;
using System.Collections;

public class Lights : MonoBehaviour {

    public KeyCode OnOff;
    public Renderer HeadlightRight;
    public Renderer HeadlightLeft;
    public Material LightsOn;
    public Material LightsOff;
    public Light LightRight;
    public Light LightLeft;

    private bool isOn = false;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(OnOff))
        {
            isOn = !isOn;
        }
        if (isOn)
        {
            HeadlightLeft.material = LightsOn;
            HeadlightRight.material = LightsOn;
            LightRight.intensity = 8f;
            LightLeft.intensity = 8f;
        }
        else
        {
            HeadlightLeft.material = LightsOff;
            HeadlightRight.material = LightsOff;
            LightRight.intensity = 0f;
            LightLeft.intensity = 0f;
        }
	}
}
