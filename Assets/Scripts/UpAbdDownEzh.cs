﻿using UnityEngine;
using System.Collections;

public class UpAbdDownEzh : MonoBehaviour {

    public Transform Element;
    public int speed;
    public int maxUp;

    private int num = 0;
    bool isConnected = false;
    public float CheckXCoord;
    public KeyCode Up;
    public KeyCode Down;
    public KeyCode Switch;

	// Use this for initialization
	void Start () {
        
    }

    // Update is called once per frame
    void Update()
    {
        isConnected = GameObject.Find("Stair").GetComponent<StairDownEzh>().isOpen;
        if (isConnected)
        {
            CheckXCoord = Element.localRotation.eulerAngles.x;
    //        Debug.Log(CheckXCoord);
            if (Input.GetKey(Up))
            {

                if (Element.localRotation.eulerAngles.x <= 10 || Element.localRotation.eulerAngles.x >= 360 - maxUp)
                    Element.Rotate(new Vector3(-5, 0, 0), speed * Time.deltaTime);
            }
            else if (Input.GetKey(Down))
            {

                if (Element.localRotation.x <= -0.01 || Element.localRotation.x >= 0.01)
                    Element.Rotate(new Vector3(5, 0, 0), speed * Time.deltaTime);
                else
                    Element.Rotate(Vector3.right, -Element.localEulerAngles.x);

            }
        }
    }
}
