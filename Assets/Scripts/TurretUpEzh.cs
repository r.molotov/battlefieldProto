﻿using UnityEngine;
using System.Collections;

public class TurretUpEzh : MonoBehaviour {

    public Transform rocketPod;
    public int speed;
	bool isMovingDown = true;
	bool isMovingUp = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	    if(Input.GetKey(KeyCode.DownArrow))
        {
      //      Debug.Log("DOWN!!");
			if(isMovingDown){
                Debug.Log("DOWN!!");
                isMovingUp = true;
				rocketPod.Rotate(new Vector3(5, 0, 0), speed * Time.deltaTime, Space.Self);
				if (rocketPod.localRotation.eulerAngles.x <= 0 || rocketPod.localRotation.eulerAngles.x <= 330) {
                    
					isMovingDown = false;
				}
			}
        } else {
			if (isMovingUp) {
				if (Input.GetKey(KeyCode.UpArrow))
				{
                    Debug.Log("UP!!");
                    isMovingDown = true;
                    rocketPod.Rotate(new Vector3(-5, 0, 0), speed * Time.deltaTime, Space.Self);
					if (rocketPod.localRotation.eulerAngles.z >= 135) {
						isMovingUp = false;
					}
				}
				
			}
		}
    }
}
