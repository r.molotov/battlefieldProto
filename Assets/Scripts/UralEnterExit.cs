﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class UralEnterExit : MonoBehaviour {

    public Camera PlayerCamera;
    public Transform Player;
    public Transform Vehicle;
	public Transform FireMachine;
    public Camera VehicleCamera;
    public KeyCode Enter;
    public Transform ExitPosition;

    public Component[] comps;

    private bool isIn = false;
    private bool inTrig;

	// Use this for initialization
	void Start () {
        Vehicle.GetComponent<UralEasySuspension>().enabled = false;
        Vehicle.GetComponent<UralTurretRotate>().enabled = false;
        Vehicle.GetComponent<UralRearWheelDrive>().enabled = false;
        Vehicle.GetComponent<UralUpAbdDown>().enabled = false;
		Vehicle.GetComponent<UralDoorsOpen>().enabled = false;
		Vehicle.GetComponent<UralCarLights>().enabled = false;
		//Vehicle.GetComponent<UralHoverAudio>().enabled = false;
		Vehicle.GetComponent<AudioSource>().enabled = false;
		Vehicle.GetComponent<UralSoundEngine>().enabled = false;
		Vehicle.Find("StartEngine").GetComponent<UralEkarny2>().enabled = false;
		Vehicle.Find("StopEngine").GetComponent<UralEkarny3>().enabled = false;
		Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").GetComponent<UralGun>().enabled = false;
		Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").Find("Pointlight1").GetComponent<Light>().enabled = false;
		Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").Find("Pointlight").GetComponent<Light>().enabled = false;
		Vehicle.Find("StartEngine").GetComponent<AudioSource>().enabled = false;
		Vehicle.Find("StopEngine").GetComponent<AudioSource>().enabled = false;
		Vehicle.Find("AudioSource").GetComponent<UralEkarnyBabay>().enabled = false;
		VehicleCamera.GetComponent<UralThirdPersonCamera>().enabled = false;
        VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
		VehicleCamera.GetComponent<UralChangeCamera>().enabled = false;
		VehicleCamera.enabled = false;
		PlayerCamera.enabled = true;
		
        

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
            inTrig = true;
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
            inTrig = false;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(Enter))
        {
            //вход
            if(inTrig && !isIn)
            {
                Player.gameObject.SetActive(false);
                Player.transform.parent = Vehicle;
                Vehicle.GetComponent<UralEasySuspension>().enabled = true;
				Vehicle.GetComponent<UralTurretRotate>().enabled = true;
				Vehicle.GetComponent<UralRearWheelDrive>().enabled = true;
				Vehicle.GetComponent<UralUpAbdDown>().enabled = true;
				Vehicle.GetComponent<UralDoorsOpen>().enabled = true;
				Vehicle.GetComponent<UralCarLights>().enabled = true;
				Vehicle.GetComponent<UralSoundEngine>().enabled = true;
				Vehicle.Find("StartEngine").GetComponent<UralEkarny2>().enabled = true;
				Vehicle.Find("StopEngine").GetComponent<UralEkarny3>().enabled = true;
				Vehicle.Find("StartEngine").GetComponent<AudioSource>().enabled = true;
				Vehicle.Find("StopEngine").GetComponent<AudioSource>().enabled = true;
				//Vehicle.GetComponent<UralHoverAudio>().enabled = true;
				VehicleCamera.GetComponent<UralChangeCamera>().enabled = true;
				Vehicle.GetComponent<AudioSource>().enabled = true;
				Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").GetComponent<UralGun>().enabled = true;
				Vehicle.Find("AudioSource").GetComponent<UralEkarnyBabay>().enabled = true;
				VehicleCamera.GetComponent<UralThirdPersonCamera>().enabled = true;
				VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
                Player.localPosition = ExitPosition.localPosition;
				
                PlayerCamera.enabled = false;
                VehicleCamera.enabled = true;
                isIn = true;
            }
            //выход
            else if (isIn)
            {
                Player.gameObject.SetActive(true);
                Player.transform.parent = null;
                Vehicle.GetComponent<UralEasySuspension>().enabled = false;
				Vehicle.GetComponent<UralTurretRotate>().enabled = false;
				Vehicle.GetComponent<UralRearWheelDrive>().enabled = false;
				Vehicle.GetComponent<UralUpAbdDown>().enabled = false;
				Vehicle.GetComponent<UralDoorsOpen>().enabled = false;
				Vehicle.GetComponent<UralCarLights>().enabled = false;
				Vehicle.Find("StartEngine").GetComponent<AudioSource>().enabled = false;
				Vehicle.Find("StopEngine").GetComponent<AudioSource>().enabled = false;
				//Vehicle.GetComponent<UralHoverAudio>().enabled = false;
				Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").GetComponent<UralGun>().enabled = false;
				Vehicle.Find("StartEngine").GetComponent<UralEkarny2>().enabled = false;
				Vehicle.Find("StopEngine").GetComponent<UralEkarny3>().enabled = false;
				Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").Find("Pointlight1").GetComponent<Light>().enabled = false;
				Vehicle.Find("Gun").Find("Gun01").Find("AmmPosition2").Find("Pointlight").GetComponent<Light>().enabled = false;
				Vehicle.Find("StartEngine").GetComponent<AudioSource>().enabled = false;
				Vehicle.GetComponent<UralSoundEngine>().enabled = false;
				Vehicle.Find("StopEngine").GetComponent<AudioSource>().enabled = false;
				VehicleCamera.GetComponent<UralChangeCamera>().enabled = false;
				//Vehicle.GetComponent<UralHoverAudio>().enabled = true;
                VehicleCamera.GetComponent<UralThirdPersonCamera>().enabled = false;
				Vehicle.Find("AudioSource").GetComponent<UralEkarnyBabay>().enabled = false;
                VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
				Vehicle.GetComponent<AudioSource>().enabled = false;
                PlayerCamera.enabled = true;
                VehicleCamera.enabled = false;
				isIn = false;
            }
        }


    }
}
