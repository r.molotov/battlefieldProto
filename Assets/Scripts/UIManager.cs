﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEngine.UI;


public class UIManager : MonoBehaviour {
    GameObject[] pauseObjects;
    bool isPaused = false;
    bool isKeyEnabled = false;
    bool inMenu = false;
	public Text text;
    // Use this for initialization
    void Start () {
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("Pause");

        hidePaused();
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
			Cursor.visible = true;
			Cursor.lockState = CursorLockMode.None;
			inMenu = true;
        }
        else
        {
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;
			inMenu = false;
        }
    }
    
	
	public void Reload () {
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void Update ()
    {
        //uses the p button to pause and unpause the game
		if (SceneManager.GetActiveScene ().name == "playground" && Input.GetKeyDown (KeyCode.Escape)) {
			if (!isPaused) showPaused ();
			else hidePaused ();
		}
    }
    public void LoadScene(string scene)
    {
        SceneManager.LoadScene(scene);
		Cursor.visible = true;
		inMenu = true;
    }
    public void showPaused()
    {
		Cursor.visible = (true);
		Cursor.lockState = CursorLockMode.None;
		isPaused = true;
        {
            foreach (GameObject g in pauseObjects)
            {
                g.SetActive(true);
            }
        }
    }
   
    //hides objects with ShowOnPause tag
    public void hidePaused()
    {
		Cursor.visible = (false);
		Cursor.lockState = CursorLockMode.Locked;
        isPaused = false;
        foreach (GameObject g in pauseObjects)
        {
            g.SetActive(false);
		}
    }
    public void Quit()
    {
        Application.Quit();
    } 
}
