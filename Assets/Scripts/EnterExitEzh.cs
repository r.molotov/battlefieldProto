﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class EnterExitEzh : MonoBehaviour {

    public Camera PlayerCamera;
    public Transform Player;
    public Transform Vehicle;
    public Camera VehicleCamera;
    public KeyCode Enter;
    public Transform ExitPosition;
    public bool isOnMusic = false;
    public float WheelAngle;
    public float WheelTorque;
    public float WheelBrake;
    public bool isIn = false;
    public bool inTrig;

	void Start () {
		WheelAngle = Vehicle.GetComponent<RearWheelDriveEzh>().maxAngle;
		WheelBrake = Vehicle.GetComponent<RearWheelDriveEzh>().maxBrake;
		WheelTorque = Vehicle.GetComponent<RearWheelDriveEzh>().maxTorque;
		Vehicle.GetComponent<RearWheelDriveEzh>().maxAngle = 0;
		Vehicle.GetComponent<RearWheelDriveEzh>().maxBrake = 1000;
		Vehicle.GetComponent<RearWheelDriveEzh>().maxTorque = 0;
        Vehicle.Find("rszo").Find("Supports").GetComponent<SupportsDownEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("Platform").Find("Platform2").GetComponent<PlatformRotateEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("Platform").Find("Platform2").Find("Stair").GetComponent<StairDownEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").GetComponent<UpAbdDownEzh>().enabled = false;

		Vehicle.Find ("rszo").Find ("turret").Find ("rocked_pod").GetComponent<SoundShotEzh> ().enabled = false;

        Vehicle.Find("rszo").Find("turret").GetComponent<LeftAndRightEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket0").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket1").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket2").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket3").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket4").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket5").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket6").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket7").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket8").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket9").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket10").GetComponent<ShotRocketEzh>().enabled = false;
        Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket11").GetComponent<ShotRocketEzh>().enabled = false;
        VehicleCamera.GetComponent<ThirdPersonCameraEzh>().enabled = false;
        VehicleCamera.enabled = false;
        PlayerCamera.enabled = true;
    }

    void OnTriggerEnter(Collider col)
    {
        inTrig = true;
    }

    void OnTriggerExit(Collider col)
    {
        inTrig = false;
    }
		
    void Update () {
        if (Input.GetKeyDown(Enter))
        {
            
            
            if(inTrig && !isIn)
            {
                isOnMusic = true;
				Debug.Log ("hello");
                Player.gameObject.SetActive(false);
                Player.transform.parent = Vehicle;

				if (!Vehicle.Find("rszo").Find("Supports").GetComponent<SupportsDownEzh>().isFinished) {
					Vehicle.GetComponent<RearWheelDriveEzh>().maxAngle = WheelAngle;
					Vehicle.GetComponent<RearWheelDriveEzh>().maxBrake = WheelBrake;
					Vehicle.GetComponent<RearWheelDriveEzh>().maxTorque = WheelTorque;
				}
                
                Vehicle.Find("rszo").Find("Supports").GetComponent<SupportsDownEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("Platform").Find("Platform2").GetComponent<PlatformRotateEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("Platform").Find("Platform2").Find("Stair").GetComponent<StairDownEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").GetComponent<UpAbdDownEzh>().enabled = true;

				Vehicle.Find ("rszo").Find ("turret").Find ("rocked_pod").GetComponent<SoundShotEzh> ().enabled = true;

                Vehicle.Find("rszo").Find("turret").GetComponent<LeftAndRightEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket0").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket1").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket2").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket3").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket4").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket5").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket6").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket7").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket8").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket9").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket10").GetComponent<ShotRocketEzh>().enabled = true;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket11").GetComponent<ShotRocketEzh>().enabled = true;
                VehicleCamera.GetComponent<ThirdPersonCameraEzh>().enabled = true;
                VehicleCamera.enabled = true;
                PlayerCamera.enabled = false;
                Player.localPosition = ExitPosition.localPosition;
                isIn = true;
            }
            //выход
			else if (isIn && !Vehicle.Find("rszo").Find("Supports").GetComponent<SupportsDownEzh>().isMoving())
            {
                isOnMusic = false;
                Player.gameObject.SetActive(true);
                Player.transform.parent = null;
				Vehicle.GetComponent<RearWheelDriveEzh>().maxAngle = 0;
				Vehicle.GetComponent<RearWheelDriveEzh>().maxBrake = 1000;
				Vehicle.GetComponent<RearWheelDriveEzh>().maxTorque = 0;
                Vehicle.Find("rszo").Find("Supports").GetComponent<SupportsDownEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("Platform").Find("Platform2").GetComponent<PlatformRotateEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("Platform").Find("Platform2").Find("Stair").GetComponent<StairDownEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").GetComponent<UpAbdDownEzh>().enabled = false;

				Vehicle.Find ("rszo").Find ("turret").Find ("rocked_pod").GetComponent<SoundShotEzh> ().enabled = false;

                Vehicle.Find("rszo").Find("turret").GetComponent<LeftAndRightEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket0").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket1").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket2").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket3").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket4").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket5").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket6").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket7").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket8").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket9").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket10").GetComponent<ShotRocketEzh>().enabled = false;
                Vehicle.Find("rszo").Find("turret").Find("rocked_pod").Find("rocket11").GetComponent<ShotRocketEzh>().enabled = false;
                VehicleCamera.GetComponent<ThirdPersonCameraEzh>().enabled = false;
                VehicleCamera.enabled = false;
                PlayerCamera.enabled = true;
                PlayerCamera.enabled = true;
                VehicleCamera.enabled = false;

                isIn = false;
            }
        }
    }
}