﻿using UnityEngine;
using System.Collections;

public class SoundShotEzh : MonoBehaviour {

    private AudioSource sound;
    // Use this for initialization
    void Start () {
        sound = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Mouse0) && GameObject.Find("rocket0").GetComponent<ShotRocketEzh>().isReady)
        {
            //          i++;
            sound.Play();
        }
    }
}
