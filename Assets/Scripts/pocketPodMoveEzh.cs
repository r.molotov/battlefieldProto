﻿using UnityEngine;
using System.Collections;

public class pocketPodMoveEzh : MonoBehaviour
{

//    public Transform target;
    private const float Y_ANGLE_MIN = -30.0f;
    private const float Y_ANGLE_MAX = 30.0f;
    private const float X_ANGLE_MIN = -30.0f;
    private const float X_ANGLE_MAX = -0.1f;

    private float curX = 0.0f;
    private float curY = 0.0f;
    public float sensX = 4.0f;
    public float sensY = 4.0f;

    private float deltaX = 0.0f;
    private float deltaY = 0.0f;

    public bool isReady = false;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        isReady = GameObject.Find("Stair").GetComponent<StairDownEzh>().isOpen;

        // float translation = Input.GetAxis("Vertical") * speed;
        //   float rotation = Input.GetAxis("Horizontal")
        //
        if (isReady)
        {

            float x1 = curX;
            float y1 = curY;

            curX += -Input.GetAxis("Vertical") * sensX;
            curY += Input.GetAxis("Horizontal") * sensY;
            curY = Mathf.Clamp(curY, Y_ANGLE_MIN, Y_ANGLE_MAX);
            curX = Mathf.Clamp(curX, X_ANGLE_MIN, X_ANGLE_MAX);

            deltaX = curX - x1;
            deltaY = curY - y1;

            transform.Rotate(deltaX, 0, 0);


            //       Transform.rotation = Quaternion.Euler(curY, curX, 0);


        }
    }
}
