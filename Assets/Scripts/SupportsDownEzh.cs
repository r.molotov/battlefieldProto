﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SupportsDownEzh : MonoBehaviour {
    float i = 0;
    [Range(0, 1)]
    public float speed = 1f;
    public float Length = 20;
    public WheelCollider[] wheels;
    public float SmerchTorque;
    public float SmerchAngle;
    public float SmerchBrake;
    Vector3 BasPos;
    public bool isFinished = false;
    public bool isMovingS = false;
    public bool isMovingBack = false;

    public bool isMoving()
    {
        foreach(var wheel in wheels)
        {
            if (wheel.rpm > 0.5)
                return true;
            
        }
        return false;
    }

	// Use this for initialization
	void Start () {
        BasPos = transform.localPosition;
		SmerchTorque = transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxTorque;
		SmerchAngle = transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxAngle;
		SmerchBrake = transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxBrake;
    }
	
	// Update is called once per frame
	void Update () {
	    if (!isMoving() && Input.GetKeyDown("1") && !isMovingBack && !isFinished && !isMovingS)
    //    if (Input.GetKeyDown("1"))
        {
            isMovingS = true;
			transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxTorque = 0;
			transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxAngle = 0;
			transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxBrake = 1000;
            i = 0;
            //         i = 0;

        }
        
        if (isMovingS)
        {
            
            if (!isFinished)
            {
                transform.Translate(Vector3.up * -Time.fixedDeltaTime* speed);
                i++;
                
            }
            
            if (i > Length / speed)
            {

                isFinished = true;
                isMovingS = false;
                
            }
        }
       

		if (!isMoving() &&
			!transform.parent.Find("turret").Find("Platform").Find("Platform2").GetComponent<PlatformRotateEzh>().isMoving &&
			!transform.parent.Find("turret").Find("Platform").Find("Platform2").GetComponent<PlatformRotateEzh>().isMovingBack &&
			!transform.parent.Find("turret").Find("Platform").Find("Platform2").GetComponent<PlatformRotateEzh>().isOpen &&
			Input.GetKeyDown("1") &&
			!isMovingS &&
			!isMovingBack)
        {
            isMovingBack = true;
            isFinished = false;
            i = 0;
			transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxTorque = SmerchTorque;
			transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxAngle = SmerchAngle;
			transform.parent.parent.GetComponent<RearWheelDriveEzh>().maxBrake = SmerchBrake;
        }

        if (isMovingBack)
        {
            if (!isFinished)
            {
                transform.Translate(Vector3.up * Time.fixedDeltaTime* speed);
                i++;

            }

            if (i > Length / speed)
            {
                isMovingBack = false;
            }
        }
    }  
}