﻿using UnityEngine;
using System.Collections;

public class DoorsOpen : MonoBehaviour {

    [Range(50, 500)]
    public int speed;

    public Transform leflDoor;
    public Transform rightDoor;
    public float closedYcord;
    public float openedYcord = 75;
    public float openedYcord1 = -75;
    bool isMoving = false;
    bool isNotOpen = true;
    bool isMoving1 = false;
    bool isNotOpen1 = true;

	// Use this for initialization
	void Start () {
        closedYcord = leflDoor.localRotation.eulerAngles.y;
	}

	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Return))
        {
            isMoving = true;
        }
        if (isMoving)
        {
            if (isNotOpen) //открываем
            {
                leflDoor.Rotate(Vector3.up, speed * Time.deltaTime, Space.Self);
                //leflDoor.RotateAroundLocal(Vector3.up, speed * Time.deltaTime);
                if (leflDoor.localRotation.eulerAngles.y >= openedYcord)
                {
                    isMoving = false;
                    isNotOpen = false;
                    leflDoor.Rotate(Vector3.up, (openedYcord - leflDoor.localRotation.eulerAngles.y));
                }
            }
            else //закрываем
            {
                leflDoor.Rotate(-Vector3.up, speed * Time.deltaTime, Space.Self);
                //leflDoor.RotateAroundLocal(-Vector3.up, speed * Time.deltaTime);
                if (leflDoor.localRotation.eulerAngles.y <= speed / 25)
                {
                    isMoving = false;
                    isNotOpen = true;
                    leflDoor.Rotate(Vector3.up, (closedYcord - leflDoor.localRotation.eulerAngles.y));
                }
            }
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            isMoving1 = true;
        }
        if (isMoving1)
        {
            if (isNotOpen1) //открываем
            {
                rightDoor.Rotate(-Vector3.up, speed * Time.deltaTime, Space.Self);
                //leflDoor.RotateAroundLocal(Vector3.up, speed * Time.deltaTime);
                if (rightDoor.localRotation.eulerAngles.y <= 295)
                {
                    isMoving1 = false;
                    isNotOpen1 = false;
                    rightDoor.Rotate(-Vector3.up, (295 - rightDoor.localRotation.eulerAngles.y));
                }
            }
            else //закрываем
            {
                rightDoor.Rotate(Vector3.up, speed * Time.deltaTime, Space.Self);
                //leflDoor.RotateAroundLocal(-Vector3.up, speed * Time.deltaTime);
                if (rightDoor.localRotation.eulerAngles.y <= speed / 25)
                {
                    isMoving1 = false;
                    isNotOpen1 = true;
                    rightDoor.Rotate(Vector3.up, (closedYcord - rightDoor.localRotation.eulerAngles.y));
                }
            }
        }


    }
}
