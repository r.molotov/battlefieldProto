﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class UIOptions : MonoBehaviour
{

    private bool showOptions = false;
    public float shadowDrawDistance;
    public float volumelevel;
    public int setShakal;
    public Slider mainSlider;
    public Slider ShakalQuality;
    //public float newvolumelevel;





    public int ResX;
    public int ResY;
    public Toggle Fullscreen;
    public Toggle Vsync;
    public Toggle Buff;
    public Toggle Ansi;
    public Dropdown resDropdown;
    GameObject[] settingsObjects;
    public Dropdown AA;

    // Use this for initialization
    void Start()
    {

        Fullscreen.isOn = Screen.fullScreen;
        mainSlider.value = AudioListener.volume;
        ShakalQuality.value = QualitySettings.GetQualityLevel();

        Resolution[] res = Screen.resolutions;

        List<string> myRess = new List<string>();
        foreach (Resolution item in res)
        {
            myRess.Add(item.ToString());
        }
        resDropdown.AddOptions(myRess);


        if (QualitySettings.vSyncCount == 0)
        {
            Vsync.isOn = false;
        }
        if (QualitySettings.maxQueuedFrames == 0)
        {
            Buff.isOn = false;
        }
        if (QualitySettings.anisotropicFiltering == AnisotropicFiltering.Disable)
        {
            Ansi.isOn = false;
        }
        if (Screen.fullScreen == false)
        {
            Fullscreen.isOn = false;
        }
        if (QualitySettings.antiAliasing == 8)
        {
            AA.value = 3;
        }
        else
        {
            AA.value = QualitySettings.antiAliasing / 2;
        }
        //Debug.Log(volumelevel);
        settingsObjects = GameObject.FindGameObjectsWithTag("Settings");
        showOptions = false;
        hideSettings();

    }

    // Update is called once per frame
    void Update()
    {
        mainSlider.value = volumelevel;
        ShakalQuality.value = setShakal;
        //AudioListener.volume = volumelevel;
        //Debug.Log(volumelevel);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            hideSettings();
        }


    }

    public void adjustVolume(float newvolumelevel)
    {
        volumelevel = newvolumelevel;
        Debug.Log(volumelevel);
        AudioListener.volume = volumelevel;

    }

    


    public void ChangeFullScreen()
    {
        Screen.fullScreen = Fullscreen.isOn;
    }

    public void ChangeVsync()
    {
        if (Vsync.isOn)
        {
            QualitySettings.vSyncCount = 1;
            Debug.Log("Vsync On");
        }
        else
        {
            QualitySettings.vSyncCount = 0;
            Debug.Log("Vsync Off");
        }
    }

    public void ChangeBuff()
    {
        if (Buff.isOn)
        {
            QualitySettings.maxQueuedFrames = 3;
            Debug.Log("Triple buffering on");
        }
        else
        {
            QualitySettings.maxQueuedFrames = 0;
            Debug.Log("Triple buffering off");
        }
    }
    public void ChangeAnsi()
    {
        if (Ansi.isOn)
        {
            QualitySettings.anisotropicFiltering = AnisotropicFiltering.ForceEnable;
            Debug.Log("Force enable anisotropic filtering!");
        }
        else
        {
            QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
            Debug.Log("Disable anisotropic filtering!");
        }
    }

    public void ChangeRes()
    {
        string tmp = resDropdown.options[resDropdown.value].text;
        string[] tmp2 = tmp.Split('x','@');
        Screen.SetResolution(int.Parse(tmp2[0].Trim()), int.Parse(tmp2[1].Trim()), Fullscreen.isOn);
    }
    public void ChangeAA()
    {
        string tmp = AA.options[AA.value].text;
        string[] tmp2 = tmp.Split('X');
        QualitySettings.antiAliasing = int.Parse(tmp2[0]);
        Debug.Log(QualitySettings.antiAliasing + "xAA");
    }
    //void OnGUI()
    //{
    //    newvolumelevel = GUI.HorizontalSlider(new Rect(0, 0, 256, 32), newvolumelevel, 0.0F, 1.0F);
    //    volumelevel = newvolumelevel;

    //}

    public void showSettings()
    {
        {
            foreach (GameObject g in settingsObjects)
            {
                g.SetActive(true);
            }
        }
    }

    //hides objects with ShowOnPause tag
    public void hideSettings()
    {
        foreach (GameObject g in settingsObjects)
        {
            g.SetActive(false);
        }
    }

    public void changeQuality(float level)
    {

        setShakal = (int)level;
        QualitySettings.SetQualityLevel(setShakal, true);

    }






}
        //void OnGUI()
        //{
        //    if (GUI.Button(new Rect(500, 100, 300, 100), "Start Game"))
        //    {
        //        Application.LoadLevel(1);
        //    }
        //    if (GUI.Button(new Rect(500, 210, 300, 100), "Quit Game"))
        //    {
        //        Application.Quit();
        //    }
        //    if (GUI.Button(new Rect(500, 320, 300, 100), "Options Menu"))
        //    {
        //        showOptions = true;
        //    }
        //    if (showOptions == true)
        //    {
        //        //INCREASE QUALITY PRESET
        //        if (GUI.Button(new Rect(810, 100, 300, 100), "Increase Quality"))
        //        {
        //            QualitySettings.IncreaseLevel();
        //            Debug.Log("Increased quality");
        //        }
        //        //DECREASE QUALITY PRESET
        //        if (GUI.Button(new Rect(810, 210, 300, 100), "Decrease Quality"))
        //        {
        //            QualitySettings.DecreaseLevel();
        //            Debug.Log("Decreased quality");
        //        }
        //        //0 X AA SETTINGS
        //        if (GUI.Button(new Rect(810, 320, 65, 100), "No AA"))
        //        {
        //            QualitySettings.antiAliasing = 0;
        //            Debug.Log("0 AA");
        //        }
        //        //2 X AA SETTINGS
        //        if (GUI.Button(new Rect(879, 320, 65, 100), "2x AA"))
        //        {
        //            QualitySettings.antiAliasing = 2;
        //            Debug.Log("2 x AA");
        //        }
        //        //4 X AA SETTINGS
        //        if (GUI.Button(new Rect(954, 320, 65, 100), "4x AA"))
        //        {
        //            QualitySettings.antiAliasing = 4;
        //            Debug.Log("4 x AA");
        //        }
        //        //8 x AA SETTINGS
        //        if (GUI.Button(new Rect(1028, 320, 65, 100), "8x AA"))
        //        {
        //            QualitySettings.antiAliasing = 8;
        //            Debug.Log("8 x AA");
        //        }
        //        //TRIPLE BUFFERING SETTINGS
        //        if (GUI.Button(new Rect(810, 430, 140, 100), "Triple Buffering On"))
        //        {
        //            QualitySettings.maxQueuedFrames = 3;
        //            Debug.Log("Triple buffering on");
        //        }
        //        if (GUI.Button(new Rect(955, 430, 140, 100), "Triple Buffering Off"))
        //        {
        //            QualitySettings.maxQueuedFrames = 0;
        //            Debug.Log("Triple buffering off");
        //        }
        //        //ANISOTROPIC FILTERING SETTINGS
        //        if (GUI.Button(new Rect(190, 100, 300, 100), "Anisotropic Filtering On"))
        //        {
        //            QualitySettings.anisotropicFiltering = AnisotropicFiltering.ForceEnable;
        //            Debug.Log("Force enable anisotropic filtering!");
        //        }
        //        if (GUI.Button(new Rect(190, 210, 300, 100), "Anisotropic Filtering Off"))
        //        {
        //            QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
        //            Debug.Log("Disable anisotropic filtering!");
        //        }
        //        //RESOLUTION SETTINGS
        //        //60Hz
        //        if (GUI.Button(new Rect(190, 320, 300, 100), "60Hz"))
        //        {
        //            Screen.SetResolution(ResX, ResY, Fullscreen, 60);
        //            Debug.Log("60Hz");
        //        }
        //        //120Hz
        //        if (GUI.Button(new Rect(190, 430, 300, 100), "120Hz"))
        //        {
        //            Screen.SetResolution(ResX, ResY, Fullscreen, 120);
        //            Debug.Log("120Hz");
        //        }
        //        //1080p
        //        if (GUI.Button(new Rect(500, 430, 93, 100), "1080p"))
        //        {
        //            Screen.SetResolution(1920, 1080, Fullscreen);
        //            ResX = 1920;
        //            ResY = 1080;
        //            Debug.Log("1080p");
        //        }
        //        //720p
        //        if (GUI.Button(new Rect(596, 430, 93, 100), "720p"))
        //        {
        //            Screen.SetResolution(1280, 720, Fullscreen);
        //            ResX = 1280;
        //            ResY = 720;
        //            Debug.Log("720p");
        //        }
        //        //480p
        //        if (GUI.Button(new Rect(692, 430, 93, 100), "480p"))
        //        {
        //            Screen.SetResolution(640, 480, Fullscreen);
        //            ResX = 640;
        //            ResY = 480;
        //            Debug.Log("480p");
        //        }
        //        if (GUI.Button(new Rect(500, 0, 140, 100), "Vsync On"))
        //        {
        //            QualitySettings.vSyncCount = 1;
        //        }
        //        if (GUI.Button(new Rect(645, 0, 140, 100), "Vsync Off"))
        //        {
        //            QualitySettings.vSyncCount = 0;
        //        }
        //    }
        //}
//    }
//}