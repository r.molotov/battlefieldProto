﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class UralCameraScript : MonoBehaviour {

    public Transform[] ViewPoints;
    public Camera cam;

    private int num;

	// Use this for initialization
	void Awake() {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            num++;
            if (num >= ViewPoints.Length)
                num = 0;
            //if (ViewPoints[num].name == "Tunguska")
            //    cam.GetComponent<ThirdPersonCamera>().enabled = true;
            //else cam.GetComponent<ThirdPersonCamera>().enabled = false;
            //if (ViewPoints[num].name == "ViewPoint3")
            //    cam.fieldOfView = 60;
            //else cam.fieldOfView = 30;
        }
        if (ViewPoints[num].name == "Cabina")
            cam.GetComponent<UralThirdPersonCamera>().enabled = true;
        else cam.GetComponent<UralThirdPersonCamera>().enabled = false;
        if (ViewPoints[num].name == "CameraPosition")
        {
            cam.GetComponent<SimpleMouseRotator>().enabled = true;
            cam.fieldOfView = 60;
        }
        else {
            cam.GetComponent<SimpleMouseRotator>().enabled = false;
            cam.fieldOfView = 30;
        }
    }
	
	void LateUpdate () {
        cam.transform.parent = ViewPoints[num];
        cam.transform.localPosition = Vector3.zero;
        cam.transform.localRotation = Quaternion.identity;
    }
}
