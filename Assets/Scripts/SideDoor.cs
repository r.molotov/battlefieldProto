﻿using UnityEngine;
using System.Collections;

public class SideDoor : MonoBehaviour {
    bool isOpen = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyUp(KeyCode.Tab))
        {
            if (isOpen)
                transform.localPosition += 6 * Vector3.forward;
            else
                transform.localPosition += 6 * Vector3.back;
            isOpen = !isOpen;
        }        
	}
}
