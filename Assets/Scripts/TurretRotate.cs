﻿using UnityEngine;
using System.Collections;

public class TurretRotate : MonoBehaviour {

    public Transform turret;
    public int speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKey(KeyCode.LeftArrow))
        {
            turret.Rotate(new Vector3(0, -5, 0), speed * Time.deltaTime, Space.Self);
        } else
        if (Input.GetKey(KeyCode.RightArrow))
        {
            turret.Rotate(new Vector3(0, 5, 0), speed * Time.deltaTime, Space.Self);
        }
    }
}
