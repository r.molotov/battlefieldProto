﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class HeliEnterExit : MonoBehaviour
{

    public Camera PlayerCamera;
    public Transform Player;
    public Transform Vehicle;
    public Camera VehicleCamera;
    public KeyCode Enter;
    public Transform ExitPosition;

    public Component[] comps;

    private bool isIn = false;
    private bool inTrig = false;

    // Use this for initialization
    void Awake()
    {
        Vehicle.GetComponent<HairyCopter>().enabled = false;
        VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = false;
        VehicleCamera.GetComponent<HeliChangeCamera>().enabled = false;
        VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
        Vehicle.Find("DoorLSide").GetComponent<SideDoor>().enabled = false;
        Vehicle.GetComponent<AudioSource>().enabled = false;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            inTrig = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            inTrig = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(Enter))
        {
            //вход
            if (inTrig && !isIn)
            {
                Player.gameObject.SetActive(false);
                Player.transform.parent = Vehicle;

                Vehicle.GetComponent<HairyCopter>().enabled = true;
                VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = true;
                VehicleCamera.GetComponent<HeliChangeCamera>().enabled = true;
                VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = true;
                Vehicle.Find("DoorLSide").GetComponent<SideDoor>().enabled = true;
                Vehicle.GetComponent<AudioSource>().enabled = true;

                Vehicle.GetComponent<AudioListener>().enabled = true;
                
                Player.localPosition = ExitPosition.localPosition;
                PlayerCamera.enabled = false;
                VehicleCamera.enabled = true;
                
                isIn = true;
            }
            //выход
            else if (isIn && Vehicle.GetComponent<HairyCopter>().EngineValue == 0)
            {
                Player.gameObject.SetActive(true);
                Player.transform.parent = null;

                Vehicle.GetComponent<HairyCopter>().enabled = false;
                VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = false;
                VehicleCamera.GetComponent<HeliChangeCamera>().enabled = false;
                VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
                Vehicle.Find("DoorLSide").GetComponent<SideDoor>().enabled = false;
                Vehicle.GetComponent<AudioSource>().enabled = false;

                Vehicle.GetComponent<AudioListener>().enabled = false;
                
                PlayerCamera.enabled = true;
                VehicleCamera.enabled = false;
                
                isIn = false;
            }
        }
    }
}
