﻿using UnityEngine;
using System.Collections;

public class LeftAndRightEzh : MonoBehaviour {
    //    public Transform Element;
    //    public int speed;
    //    public int maxUp;

    //    private int num = 0;
    public float CheckYCoord;
    public KeyCode Left;
    public KeyCode Right;
//    public KeyCode Switch;
//    float openedYcoord = 260;
    float closedYcoord;
    public Transform platform;
    public bool isMoving = false;
    public bool isOpenR = true;
    public bool isOpenL = true;
    public bool isConnected = false;
    public float speed = 10;
    public float ankle = 80;
    // Use this for initialization
    void Start()
    {


        closedYcoord = platform.localRotation.eulerAngles.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameObject.Find("Stair").GetComponent<StairDownEzh>().isOpen && !GameObject.Find("Stair").GetComponent<StairDownEzh>().isMoving && !GameObject.Find("Stair").GetComponent<StairDownEzh>().isMovingBack)
        {
            isConnected = true;
        }
        //       Debug.Log(isConnected);
        if (GameObject.Find("Stair").GetComponent<StairDownEzh>().isOpen && !GameObject.Find("Stair").GetComponent<StairDownEzh>().isMoving && !GameObject.Find("Stair").GetComponent<StairDownEzh>().isMovingBack)
        {
            isConnected = true;
        }

        if (isConnected)
        {
            isMoving = true;

        }
        if (isMoving)
        {
            if(Input.GetKey(Left) && isOpenL)
                 transform.Rotate(Vector3.down * Time.deltaTime * speed);
            if (Input.GetKey(Right) && isOpenR)
                transform.Rotate(Vector3.up * Time.deltaTime * speed);

        }
     //   X = platform.localRotation.eulerAngles.y;
        CheckYCoord = platform.localRotation.eulerAngles.y;
     //   Debug.Log("Y(turret) = " + CheckYCoord);
        if (platform.localRotation.eulerAngles.y >= ankle && platform.localRotation.eulerAngles.y <= (360 - ankle) && isMoving)
        {
            isOpenL = false;
            isOpenR = false;
        }
        else {
            isOpenR = true;
            isOpenL = true;
        }
        if (!isOpenL && !isOpenR && isMoving)
        {
            if (platform.localRotation.eulerAngles.y >= (ankle - 10) && platform.localRotation.eulerAngles.y <= (ankle + 10))
                isOpenL = true;
            if (platform.localRotation.eulerAngles.y >= (360 - ankle - 10) && platform.localRotation.eulerAngles.y <= (360 - ankle + 10))
                isOpenR = true;
        }
        

        
    }
}
