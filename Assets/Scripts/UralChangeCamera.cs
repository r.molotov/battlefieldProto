﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class UralChangeCamera : MonoBehaviour {

	// Use this for initialization
    public Camera camera;
    public Transform[] viewPoints;
	public Transform Vehicle;
    public int currView = 0;
	void Start () {
        camera.transform.parent = viewPoints[currView];
        camera.transform.localPosition = Vector3.zero;
        camera.transform.localEulerAngles = Vector3.zero;
	}
	
    void LateUpdate()
    {
        if (Input.GetKeyUp(KeyCode.C))
        {
            
            if (currView == viewPoints.Length-1) currView = 0;
            else currView++;
            

            camera.transform.parent = viewPoints[currView];
            camera.transform.localPosition = Vector3.zero;
            camera.transform.localEulerAngles = Vector3.zero;

            if (currView > 0)
            {
                camera.GetComponent<UralThirdPersonCamera>().enabled = false;
                camera.GetComponent<SimpleMouseRotator>().enabled = true;
            }
            else
            {
                camera.GetComponent<UralThirdPersonCamera>().enabled = true;
                camera.GetComponent<SimpleMouseRotator>().enabled = false;
            }
        }
    }
}
