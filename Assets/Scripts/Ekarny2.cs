﻿using UnityEngine;
using System.Collections;

public class Ekarny2 : MonoBehaviour
{

    public AudioSource Ekarny;
    private bool Zapuschen = false;
    // Use this for initialization
    void Start()
    {
        Ekarny = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !Zapuschen)
        {
            GetComponent<AudioSource>().Play();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            Zapuschen = !Zapuschen;
        }
    }
}
