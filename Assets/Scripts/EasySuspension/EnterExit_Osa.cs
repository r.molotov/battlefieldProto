﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class EnterExit_Osa : MonoBehaviour {

    public Camera PlayerCamera;
    public Transform Player;
    public Transform Vehicle;
    public Camera VehicleCamera;
    public KeyCode Enter;
    public Transform ExitPosition;

    public Component[] comps;

    private bool isIn = false;
    private bool inTrig;

	// Use this for initialization
	void Start () {
        //Vehicle.GetComponent<EasySuspension_Osa>().enabled = false;
        Vehicle.GetComponent<CarLights_Osa>().enabled = false;
        Vehicle.GetComponent<RearWheelDrive_Osa>().enabled = false;  
		Vehicle.GetComponent<AudioSource>().enabled = false;
        Vehicle.Find("turret").GetComponent<TurretRotate_Osa>().enabled = false;
		VehicleCamera.GetComponent<ThirdPersonCamera_Osa>().enabled = false;
        Vehicle.Find("turret").Find("Antenna").GetComponent<Antenna_turret_Osa>().enabled = false;
        Vehicle.Find("chassis").Find("DriverHatch").GetComponent<driver_hatch_Osa>().enabled = false;
        
        Vehicle.Find("chassis").Find("TurboHatch").GetComponent<TurboHatch_Osa>().enabled = false;
        Vehicle.Find("chassis").Find("WaveSplitter").GetComponent<SplitterRotate_Osa>().enabled = false;
        Vehicle.Find("turret").Find("LockerLB").GetComponent<WeaponLeft_OsaBack>().enabled = false;
        Vehicle.Find("turret").Find("LockerLF").GetComponent<WeaponLeft_Osa>().enabled = false;
        Vehicle.Find("turret").Find("LockerRB").GetComponent<WeaponRight_OsaBack>().enabled = false;
        Vehicle.Find("turret").Find("LockerRF").GetComponent<WeaponRight_Osa>().enabled = false;
        Vehicle.Find("turret").Find("radars").GetComponent<Radar_turret_Osa>().enabled = false;

        //Vehicle.GetComponent<Animator>().enabled = false;

        //VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
        //VehicleCamera.GetComponent<ChangeCamera>().enabled = false;
        VehicleCamera.enabled = false;
		
        

    }

    void OnTriggerEnter(Collider col)
    {
        inTrig = true;
    }

    void OnTriggerExit(Collider col)
    {
        inTrig = false;
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(Enter))
        {
            //вход
            if(inTrig && !isIn)
            {
                Player.gameObject.SetActive(false);
                Player.transform.parent = Vehicle;
                //Vehicle.GetComponent<EasySuspension_Osa>().enabled = true;

				Vehicle.GetComponent<RearWheelDrive_Osa>().enabled = true;
                Vehicle.Find("turret").GetComponent<TurretRotate_Osa>().enabled = true;
				Vehicle.GetComponent<CarLights_Osa>().enabled = true;
                Vehicle.Find("turret").Find("Antenna").GetComponent<Antenna_turret_Osa>().enabled = true;
                Vehicle.Find("chassis").Find("DriverHatch").GetComponent<driver_hatch_Osa>().enabled = true;

                Vehicle.Find("chassis").Find("TurboHatch").GetComponent<TurboHatch_Osa>().enabled = true;
                Vehicle.Find("chassis").Find("WaveSplitter").GetComponent<SplitterRotate_Osa>().enabled = true;
                Vehicle.Find("turret").Find("LockerLB").GetComponent<WeaponLeft_OsaBack>().enabled = true;
                Vehicle.Find("turret").Find("LockerLF").GetComponent<WeaponLeft_Osa>().enabled = true;
                Vehicle.Find("turret").Find("LockerRB").GetComponent<WeaponRight_OsaBack>().enabled = true;
                Vehicle.Find("turret").Find("LockerRF").GetComponent<WeaponRight_Osa>().enabled = true;
                Vehicle.Find("turret").Find("radars").GetComponent<Radar_turret_Osa>().enabled = true;
				
			//	VehicleCamera.GetComponent<ChangeCamera>().enabled = true;
				Vehicle.GetComponent<AudioSource>().enabled = true;


                //Vehicle.GetComponent<Animator>().enabled = true;


                VehicleCamera.GetComponent<ThirdPersonCamera_Osa>().enabled = true;
                Player.localPosition = ExitPosition.localPosition;
                PlayerCamera.enabled = false;
                VehicleCamera.enabled = true;
                isIn = true;
            }
            //выход
            else if (isIn)
            {
                Player.gameObject.SetActive(true);
                Player.transform.parent = null;
                //Vehicle.GetComponent<EasySuspension_Osa>().enabled = false;

				Vehicle.GetComponent<RearWheelDrive_Osa>().enabled = false;
                Vehicle.Find("turret").GetComponent<TurretRotate_Osa>().enabled = false;
                Vehicle.Find("turret").Find("Antenna").GetComponent<Antenna_turret_Osa>().enabled = false;
				Vehicle.GetComponent<CarLights_Osa>().enabled = false;
                Vehicle.Find("chassis").Find("DriverHatch").GetComponent<driver_hatch_Osa>().enabled = false;

                Vehicle.Find("chassis").Find("TurboHatch").GetComponent<TurboHatch_Osa>().enabled = false;
                Vehicle.Find("chassis").Find("WaveSplitter").GetComponent<SplitterRotate_Osa>().enabled = false;
                Vehicle.Find("turret").Find("LockerLB").GetComponent<WeaponLeft_OsaBack>().enabled = false;
                Vehicle.Find("turret").Find("LockerLF").GetComponent<WeaponLeft_Osa>().enabled = false;
                Vehicle.Find("turret").Find("LockerRB").GetComponent<WeaponRight_OsaBack>().enabled = false;
                Vehicle.Find("turret").Find("LockerRF").GetComponent<WeaponRight_Osa>().enabled = false;
                Vehicle.Find("turret").Find("radars").GetComponent<Radar_turret_Osa>().enabled = false;
				
				Vehicle.GetComponent<AudioSource>().enabled = false;

                VehicleCamera.GetComponent<ThirdPersonCamera_Osa>().enabled = false;


                //Vehicle.GetComponent<Animator>().enabled = false;

                //VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
                PlayerCamera.enabled = true;
                VehicleCamera.enabled = false;
                isIn = false;
            }
        }


    }
}
