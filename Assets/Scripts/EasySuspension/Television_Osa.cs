﻿using UnityEngine;
using System.Collections;

public class Television_Osa : MonoBehaviour {

    public Transform television;
    public int speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKey(KeyCode.X))
        {
            television.Rotate(new Vector3(-5, 0, 0), speed * Time.deltaTime, Space.Self);
        } else
        if (Input.GetKey(KeyCode.C))
        {
            television.Rotate(new Vector3(5, 0, 0), speed * Time.deltaTime, Space.Self);
        }
    }
}
