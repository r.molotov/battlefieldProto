﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RearWheelDriveEzh : MonoBehaviour {

	public WheelCollider[] wheelsT;
	public WheelCollider[] wheelsD;

	public float maxAngle = 30;
	public float maxTorque = 25;
	public float maxBrake = 50;

	// here we find all the WheelColliders down in the hierarchy
	public void Start()
	{
		GetComponent<Rigidbody>().centerOfMass = new Vector3(0, 1, 0);
	}

	// this is a really simple approach to updating wheels
	// here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
	// this helps us to figure our which wheels are front ones and which are rear
	public void FixedUpdate()
	{
		float angle = maxAngle * Input.GetAxis("Horizontal");
		float torque = maxTorque * Input.GetAxis("Vertical");

		foreach (WheelCollider wheel in wheelsD)
		{
			if (Input.GetKeyDown("space"))
			{
				wheel.brakeTorque = maxBrake;
				wheel.motorTorque = 0;
			}
			else if (Input.GetKeyUp("space"))
				wheel.brakeTorque = 0;

			// update visual wheels if any
			Quaternion q;
			Vector3 p;
			wheel.GetWorldPose(out p, out q);

			// assume that the only child of the wheelcollider is the wheel shape
			Transform shapeTransform = wheel.transform.GetChild(0);
			shapeTransform.position = p;
			shapeTransform.rotation = q;

			if (!Input.GetKey("space"))
			{
				wheel.motorTorque = torque;
			}
		}

		foreach (WheelCollider wheel in wheelsT)
		{
			if (Input.GetKey("space"))
			{
				wheel.brakeTorque = maxBrake;
				wheel.motorTorque = 0;
			}
			else if (Input.GetKeyUp("space"))
				wheel.brakeTorque = 0;

			wheel.steerAngle = angle;

			// update visual wheels if any
			Quaternion q;
			Vector3 p;
			wheel.GetWorldPose(out p, out q);

			// assume that the only child of the wheelcollider is the wheel shape
			Transform shapeTransform = wheel.transform.GetChild(0);
			shapeTransform.position = p;
			shapeTransform.rotation = q;
		}
	}
}
