﻿using UnityEngine;
using System.Collections;

public class SoundEngine_Osa : MonoBehaviour {

    public Rigidbody rb;
    private AudioSource a;
    public float volume;
    public bool dvizhok = false;
        // Use this for initialization
	void Start () {
            a = GetComponent<AudioSource>();
            rb.centerOfMass = new Vector3(0, 0, 0);
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R) && !dvizhok)
        {
            GetComponent<AudioSource>().Play();
        }
        a.volume = Mathf.Clamp(rb.velocity.magnitude / 14, 0.3f, 0.7f);
        Debug.Log(rb.velocity.magnitude);
        if (Input.GetKeyDown(KeyCode.R) && dvizhok)
        {
            GetComponent<AudioSource>().Stop();
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            dvizhok = !dvizhok;
        }
    }
}
