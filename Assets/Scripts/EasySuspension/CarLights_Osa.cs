﻿using UnityEngine;
using System.Collections;

public class CarLights_Osa : MonoBehaviour
{

    public Renderer[] brakeLights;
    public Material brakeLightON;
    public Material brakeLightOFF;
    public Material leftLightON;
    public Material leftLightOFF;
    public Renderer[] headLights;
    public Material headLightsON;
    public Material headLightsOFF;
    public Light SpolightLEFT;
    public Light SpolightRIGHT;
    private bool rightSignalON = false;
    private bool leftSignalON = false;
    void Start()
    {


    }

    void Update()
    {

        if (Input.GetKey(KeyCode.S))
        {
            brakeLights[0].material = brakeLightON;
        }
        else {
            brakeLights[0].material = brakeLightOFF;
        }

        if (Input.GetKey(KeyCode.D))
        {
            brakeLights[2].material = leftLightON;
            rightSignalON = true;
            leftSignalON = false;
        }
        else {
            brakeLights[2].material = leftLightOFF;
            rightSignalON = false;
            leftSignalON = false;
        }


        if (Input.GetKey(KeyCode.A))
        {
            brakeLights[1].material = leftLightON;
            rightSignalON = false;
            leftSignalON = true;

        }
        else {
            brakeLights[1].material = leftLightOFF;
            rightSignalON = false;
            leftSignalON = false;
        }
        if (leftLightON)
        {
            float floor = 0f;
            float ceiling = 1f;
            float emission = floor + Mathf.PingPong(Time.time * 2f, ceiling - floor);
            brakeLights[1].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
            brakeLights[2].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
        }

        if (Input.GetKeyDown("1"))
        {
            headLights[0].material = headLightsON;
            SpolightLEFT.intensity = 5f;
            SpolightRIGHT.intensity = 5f;
        }

        if (Input.GetKeyDown("2"))
        {
            headLights[0].material = headLightsOFF;
            SpolightLEFT.intensity = 0f;
            SpolightRIGHT.intensity = 0f;
        }
    }
}
