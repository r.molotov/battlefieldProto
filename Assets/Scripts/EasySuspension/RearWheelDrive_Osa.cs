﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RearWheelDrive_Osa : MonoBehaviour {

	public WheelCollider[] wheelsF;
    public WheelCollider[] wheelsR;
    public WheelCollider[] wheelsB;

	public float maxAngle = 30;
    public float maxAngleB = 30;
	public float maxTorque = 25;
    public float maxBrake = 50;

	// here we find all the WheelColliders down in the hierarchy
	public void Start()
	{

	}
	// this is a really simple approach to updating wheels
	// here we simulate a rear wheel drive car and assume that the car is perfectly symmetric at local zero
	// this helps us to figure our which wheels are front ones and which are rear
	public void FixedUpdate()
	{
		float angle = maxAngle * Input.GetAxis("Horizontal");
        float angleB = -maxAngleB * Input.GetAxis("Horizontal");
		float torque = maxTorque * Input.GetAxis("Vertical");

		foreach (WheelCollider wheel in wheelsR)
		{
            if (Input.GetKeyDown("space"))
            {
                wheel.brakeTorque = maxBrake;
                wheel.motorTorque = 0;
            }
            else if (Input.GetKeyUp("space"))
                wheel.brakeTorque = 0;

            // update visual wheels if any
            Quaternion q;
            Vector3 p;
            wheel.GetWorldPose(out p, out q);

            // assume that the only child of the wheelcollider is the wheel shape
            Transform shapeTransform = wheel.transform.GetChild(0);
            shapeTransform.position = p;
            shapeTransform.rotation = q;

            if (!Input.GetKey("space"))
            {
                wheel.motorTorque = torque;
            }
		}

        foreach (WheelCollider wheel in wheelsF)
        {
            if (Input.GetKey("space"))
            {
                wheel.brakeTorque = maxBrake;
                wheel.motorTorque = 0;
            }
            else if (Input.GetKeyUp("space"))
                wheel.brakeTorque = 0;

            wheel.steerAngle = angle;

            // update visual wheels if any
            Quaternion q;
            Vector3 p;
            wheel.GetWorldPose(out p, out q);

            // assume that the only child of the wheelcollider is the wheel shape
            Transform shapeTransform = wheel.transform.GetChild(0);
            shapeTransform.position = p;
            shapeTransform.rotation = q;
        }

        foreach (WheelCollider wheel in wheelsB)
        {
            if (Input.GetKey("space"))
            {
                wheel.brakeTorque = maxBrake;
                wheel.motorTorque = 0;
            }
            else if (Input.GetKeyUp("space"))
                wheel.brakeTorque = 0;

            wheel.steerAngle = angleB;

            // update visual wheels if any
            Quaternion q;
            Vector3 p;
            wheel.GetWorldPose(out p, out q);

            // assume that the only child of the wheelcollider is the wheel shape
            Transform shapeTransform = wheel.transform.GetChild(0);
            shapeTransform.position = p;
            shapeTransform.rotation = q;
        }
	}
}
