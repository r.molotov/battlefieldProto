﻿using UnityEngine;
using System.Collections;


public class Gun_Osa : MonoBehaviour {
	
	public Transform amm;
	public GameObject PosAmm;
	//public GameObject point;
	//public GameObject point1;
	public int speedAmm = 800;
	int i = 0;
	int j = 0;
	const float Z = 84.1f;
	const float X = 28f;
	const float Y = 35.9f;
	const float Del = 6f;
	bool isFire = false;
	void Start() {
		
	
	}

	void Fire() {
		
		if(Input.GetKeyUp(KeyCode.N)) {


			Transform g = (Transform) Instantiate(amm, transform.position, transform.rotation);
			PosAmm.transform.localPosition = new Vector3(X - i * Del, Y + j * Del, Z);
			g.GetComponent<Rigidbody>().AddForce(transform.forward * speedAmm);
			//point.GetComponent<Light>().enabled = true;
			//point1.GetComponent<Light>().enabled = true;
			i++;
			if (i >= 9) {
				i = 0;
				j++;
			}
			if (j >= 4) {
				i = 0;
				j = 0;
			} 


		}
		else {
			//point.GetComponent<Light>().enabled = false;
			//point1.GetComponent<Light>().enabled = false;
		}

		if (Input.GetKeyUp(KeyCode.M)) {
			isFire = true;
		
		}

	}

	void Update() {
		Fire ();
		if (isFire) {
			StartCoroutine(FireWithPause());
		}
	}

	private IEnumerator FireWithPause() {
		
		Transform g = (Transform) Instantiate(amm, transform.position, transform.rotation);
		PosAmm.transform.localPosition = new Vector3(X - i * Del, Y + j * Del, Z);
		g.GetComponent<Rigidbody>().AddForce(transform.forward * speedAmm);
		//point.GetComponent<Light>().enabled = true;
		//point1.GetComponent<Light>().enabled = true;
		i++;
		if (i >= 9) {
			i = 0;
			j++;
		}
		if (j >= 4) {
			i = 0;
			j = 0;
		} 
		yield return new WaitForSeconds(10f);

	}
    

}
