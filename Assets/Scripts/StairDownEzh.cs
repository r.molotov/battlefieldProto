﻿using UnityEngine;
using System.Collections;

public class StairDownEzh : MonoBehaviour {

    // Use this for initialization
    float openedYcoord = 90;
    float closedYcoord;
    public Transform stair;
    public bool isMoving = false;
    public bool isOpen = false;
    bool isConnected = false;
    public float speed = 10;
    public bool isMovingBack;
    // Use this for initialization
    void Start()
    {


        closedYcoord = stair.localRotation.eulerAngles.x;
    }
   
    bool isXReady()
    {
        if ((GameObject.Find("rocked_pod").GetComponent<UpAbdDownEzh>().CheckXCoord > 359 && GameObject.Find("rocked_pod").GetComponent<UpAbdDownEzh>().CheckXCoord < 360) || (GameObject.Find("rocked_pod").GetComponent<UpAbdDownEzh>().CheckXCoord > -0.5 && GameObject.Find("rocked_pod").GetComponent<UpAbdDownEzh>().CheckXCoord < 0.5))
            return true;
        return false;
    }

    bool isYReady()
    {
		if ((transform.parent.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord > 357 &&
				transform.parent.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord < 360) ||
			(transform.parent.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord > -0.5 &&
				transform.parent.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord < 1.5))
            return true;
        return false;
    }

     bool isReadyBack()
    {
  //      Debug.Log("X = " + isXReady() + " Y = " + isYReady());
        if (isXReady() && isYReady())
            return true;
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        isConnected = GameObject.Find("Platform2").GetComponent<PlatformRotateEzh>().isOpen;
  //      bool X = isXReady();
  //      Debug.Log(X);

        if (Input.GetKeyDown("3") && isConnected && !isOpen && !isMovingBack && !isMoving)
        {
            isMoving = true;

        }
        if (isMoving)
        {
            //        Debug.Log("Moving!!!");
            transform.Rotate(Vector3.up * Time.deltaTime * speed);
        }
        if (stair.localRotation.eulerAngles.y >= openedYcoord)
        {
            isMoving = false;
            isOpen = true;
        }

        if (isOpen && isReadyBack() && Input.GetKeyDown("3") && !isMovingBack && !isMoving)
        {
            isMovingBack = true;
            isOpen = false;
            //       GameObject.Find("Stair").GetComponent<StairDown>().isOpen
			transform.parent.parent.parent.GetComponent<LeftAndRightEzh>().isConnected = false;
			transform.parent.parent.parent.GetComponent<LeftAndRightEzh>().isMoving = false;

        }

        if (isMovingBack)
        {
            transform.Rotate(Vector3.down * Time.deltaTime * speed);
     //       Debug.Log(stair.localRotation.eulerAngles.y);
            if (stair.localRotation.eulerAngles.y < 1)
            {
                isMovingBack = false;
            }
        }
    }
}
