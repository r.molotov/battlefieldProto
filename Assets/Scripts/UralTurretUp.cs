﻿using UnityEngine;
using System.Collections;

public class UralTurretUp : MonoBehaviour {

    public Transform turret;
    public int speed;
	bool isMovingDown = true;
	bool isMovingUp = true;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	    if(Input.GetKey(KeyCode.DownArrow))
        {
			isMovingUp = true;
			if(isMovingDown){
				turret.Rotate(new Vector3(5, 0, 0), speed * Time.deltaTime, Space.Self);
				if (turret.localRotation.eulerAngles.x >= 357) {
					isMovingDown = false;
				}
			}
        } else {
			if (Input.GetKey(KeyCode.UpArrow))
				{
					isMovingDown = true;
					if (isMovingUp) {
						turret.Rotate(new Vector3(-5, 0, 0), speed * Time.deltaTime, Space.Self);
						if (turret.localRotation.eulerAngles.x <= 307) {
							isMovingUp = false;
						}
				}
				
			}
		}
    }
}
