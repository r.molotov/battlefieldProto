﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Utility;

public class MatveevEnterExit : MonoBehaviour {

    public Camera PlayerCamera;
    public Transform Player;
    public Transform Vehicle;
    public Camera VehicleCamera;
    public KeyCode Enter;
    public Transform ExitPosition;

    public Component[] comps;

    private bool isIn = false;
    private bool inTrig = false;

	// Use this for initialization
	void Awake () {
        Vehicle.GetComponent<DoorsOpen>().enabled = false;
        Vehicle.GetComponent<EasySuspension>().enabled = false;
        Vehicle.GetComponent<RearWheelDrive>().enabled = false;
        Vehicle.GetComponent<CarLights_Matveev>().enabled = false;
        Vehicle.GetComponent<SoundEngine>().enabled = false;
        Vehicle.GetComponent<ChangeCamera>().enabled = false;
		Vehicle.GetComponent<AudioSource>().enabled = false;		
        VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = false;
        Vehicle.Find("DrawCall_1226").GetComponent<EkarnyBabay>().enabled = false;
        Vehicle.Find("starter").GetComponent<Ekarny2>().enabled = false;
        Vehicle.Find("starter").GetComponent<AudioSource>().enabled = false;
        Vehicle.Find("glushenie").GetComponent<Ekarny3>().enabled = false;
        Vehicle.Find("glushenie").GetComponent<AudioSource>().enabled = false;
        Vehicle.GetComponent<AudioListener>().enabled = false;
        VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
        VehicleCamera.enabled = false;
    }
    void OnTriggerEnter(Collider col)
    {
        if (col.tag == "Player")
        {
            inTrig = true;
        }
    }

    void OnTriggerExit(Collider col)
    {
        if (col.tag == "Player")
        {
            inTrig = false;
        }
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKeyDown(Enter))
        {
            //вход
            if(inTrig && !isIn)
            {
                Player.gameObject.SetActive(false);
                Player.transform.parent = Vehicle;
                Vehicle.GetComponent<DoorsOpen>().enabled = true;
                //Vehicle.GetComponent<EasySuspension>().enabled = true;
                Vehicle.GetComponent<RearWheelDrive>().enabled = true;
                Vehicle.GetComponent<CarLights_Matveev>().enabled = true;
                Vehicle.GetComponent<SoundEngine>().enabled = true;
                Vehicle.GetComponent<ChangeCamera>().enabled = true;
                Vehicle.GetComponent<AudioSource>().enabled = true;
                VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = true;
                Vehicle.Find("DrawCall_1226").GetComponent<EkarnyBabay>().enabled = true;
                Vehicle.Find("starter").GetComponent<Ekarny2>().enabled = true;
                Vehicle.Find("starter").GetComponent<AudioSource>().enabled = true;
                Vehicle.Find("glushenie").GetComponent<Ekarny3>().enabled = true;
                Vehicle.Find("glushenie").GetComponent<AudioSource>().enabled = true;
                VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
                Vehicle.GetComponent<AudioListener>().enabled = true;
                Player.localPosition = ExitPosition.localPosition;
                PlayerCamera.enabled = false;
                VehicleCamera.enabled = true;
                isIn = true;
            }
            //выход
            else if (isIn)
            {
                Player.gameObject.SetActive(true);
                Player.transform.parent = null;
                Vehicle.GetComponent<DoorsOpen>().enabled = false;
                Vehicle.GetComponent<EasySuspension>().enabled = false;
                Vehicle.GetComponent<RearWheelDrive>().enabled = false;
                Vehicle.GetComponent<CarLights_Matveev>().enabled = false;
                Vehicle.GetComponent<SoundEngine>().enabled = false;
                Vehicle.GetComponent<ChangeCamera>().enabled = false;
                Vehicle.GetComponent<AudioSource>().enabled = false;
                VehicleCamera.GetComponent<ThirdPersonCamera>().enabled = false;
                Vehicle.Find("DrawCall_1226").GetComponent<EkarnyBabay>().enabled = false;
                Vehicle.Find("starter").GetComponent<Ekarny2>().enabled = false;
                Vehicle.Find("starter").GetComponent<AudioSource>().enabled = false;
                Vehicle.Find("glushenie").GetComponent<Ekarny3>().enabled = false;
                Vehicle.Find("glushenie").GetComponent<AudioSource>().enabled = false;
                VehicleCamera.GetComponent<SimpleMouseRotator>().enabled = false;
                Vehicle.GetComponent<AudioListener>().enabled = false;
                PlayerCamera.enabled = true;
                VehicleCamera.enabled = false;
                isIn = false;
            }
        }
    }
}
