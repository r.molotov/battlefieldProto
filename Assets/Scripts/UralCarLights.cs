﻿using UnityEngine;
using System.Collections;

public class UralCarLights : MonoBehaviour {

    public Renderer[] brakeLights;
	public Renderer[] turnLights;
	public Material brakeLightON;
	public Material brakeLightOFF;
	public Material turnLightON;
	public Material turnLightOFF;
	public Renderer[] headLights;
	public Material headLightsON;
	public Material headLightsOFF;
	public Light SpolightLEFT;
	public Light SpolightRIGHT;
	bool isLight = false;
	bool leftLightON = false;
	bool rightLightON = false;
	
	void Start() {
		
	}
	
	void Update() {
		
		if(Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.Space))
		{
			for(int i = 0; i < brakeLights.Length; i++) 
				brakeLights[i].material = brakeLightON;
			
		}
		else{
			
			for(int i = 0; i < brakeLights.Length; i++) 
				brakeLights[i].material = brakeLightOFF;
			
		}
		
		if(Input.GetKeyDown(KeyCode.Z))
		{
				if (leftLightON) {
					leftLightON = false;
					for(int i = 0; i < turnLights.Length/ 2; i++) 
						turnLights[i].material = turnLightOFF;
					return;
				}
				
				leftLightON = true;
				rightLightON = false;
				
				for(int i = turnLights.Length / 2; i < turnLights.Length; i++) 
					turnLights[i].material = turnLightOFF;
				
				for(int i = 0; i < turnLights.Length/ 2; i++) 
					turnLights[i].material = turnLightON;
	
				
		}
		else if (Input.GetKeyDown(KeyCode.X))
		{
				if (rightLightON) {
					rightLightON = false;
					for(int i = turnLights.Length / 2; i < turnLights.Length; i++) 
						turnLights[i].material = turnLightOFF;
					return;
				}
				
				rightLightON = true;
				leftLightON = false;
				
				for(int i = 0; i < turnLights.Length/ 2; i++) 
					turnLights[i].material = turnLightOFF;
				
				for(int i = turnLights.Length / 2; i < turnLights.Length; i++) 
					turnLights[i].material = turnLightON;
				
		}
		
		
		if (leftLightON) {
			
			float floor = 0f;
			float ceiling = 0.7f;
			float emission = floor + Mathf.PingPong(Time.time * 2f, ceiling - floor);
			
			for(int i = 0; i < turnLights.Length/ 2; i++) 
					turnLights[i].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
				
		}
			
		if (rightLightON) {
			
			float floor = 0f;
			float ceiling = 0.7f;
			float emission = floor + Mathf.PingPong(Time.time * 2f, ceiling - floor);
			

			for(int i = turnLights.Length / 2; i < turnLights.Length; i++) 
					turnLights[i].material.SetColor("_EmissionColor", new Color(1f, 1f, 1f) * emission);
				
		}
		
		
		
		if(Input.GetKeyDown(KeyCode.F))
		{
			if (!isLight){
				for(int i = 0; i < headLights.Length; i++) 
					headLights[i].material = headLightsON;
			
				SpolightLEFT.intensity = 18f;
				SpolightRIGHT.intensity = 18f;
				isLight = true;;
			}
			else {
				for(int i = 0; i < headLights.Length; i++) 
					headLights[i].material = headLightsOFF;
				
				SpolightLEFT.intensity = 0f;
				SpolightRIGHT.intensity = 0f;
				isLight = false;
			}
		}
		
		
		
		
			
		
	}
}
