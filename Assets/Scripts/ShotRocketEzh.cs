﻿using UnityEngine;
using System.Collections;

public class ShotRocketEzh : MonoBehaviour {

    public Transform amm;
 //   public Transform[] rocket;
    public GameObject point;
    public int i = 0;
    public int speedAmm = 800;
    public int k = 0;
    public bool isReady = false;
	// Use this for initialization
	void Start () {
        point.GetComponent<Light>().enabled = false;
    
    }
	
	// Update is called once per frame
	void Update () {

		if ((transform.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord > 30 && transform.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord < 81) || (transform.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord > 278 && transform.parent.parent.GetComponent<LeftAndRightEzh>().CheckYCoord < 330) || (GameObject.Find("rocked_pod").GetComponent<UpAbdDownEzh>().CheckXCoord > 338 && GameObject.Find("rocked_pod").GetComponent<UpAbdDownEzh>().CheckXCoord < 347))
        {
     
                 isReady = true;
        }
        else
        {
            isReady = false;
        }
	    if (Input.GetKeyDown(KeyCode.Mouse0) && isReady)
        {
            
            if (i % 12 == k)
            {
                //      Debug.Log("______________");
                Transform g = (Transform)Instantiate(amm, transform.position, transform.rotation);

                g.GetComponent<Rigidbody>().AddForce(transform.forward * speedAmm);
                //           g.rigidbody.AddForce(transform.forward * speedAmm);
                point.GetComponent<Light>().enabled = true;
            }
            i++;
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            point.GetComponent<Light>().enabled = false;
            
        }


    }
}
