﻿using UnityEngine;
using System.Collections;

public class UralEkarny3 : MonoBehaviour
{

    public AudioSource Ekarny;
    public bool isEngine = false;
    // Use this for initialization
    void Start()
    {
        Ekarny = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.B) && isEngine)
        {
            GetComponent<AudioSource>().Play();
			isEngine = false;
        }
		
		 if (Input.GetKeyDown(KeyCode.V))
        {
           isEngine = true;
        }
    }
}
