﻿using UnityEngine;
using System.Collections;

public class UralEkarny2 : MonoBehaviour
{
	
    public AudioSource Ekarny;
	public bool isEngine = false;
    // Use this for initialization
    void Start()
    {
        Ekarny = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
		
        if (Input.GetKeyDown(KeyCode.V) && !isEngine)
        {
			GetComponent<AudioSource>().Play();
			isEngine = true;
        }
		
		if (Input.GetKeyDown(KeyCode.B)) {
			isEngine = false;
		}
		
		
    }
}
